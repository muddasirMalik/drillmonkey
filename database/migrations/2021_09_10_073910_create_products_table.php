<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable();
            $table->string('price')->nullable();
            $table->text('image')->nullable();
            $table->text('link')->nullable();
            $table->text('description')->nullable();
            $table->string('barcode')->nullable();
            $table->string('status')->nullable();
            $table->string('in_stock')->nullable();
            $table->bigInteger('store_links_id')->unsigned()->nullable();
            $table->foreign('store_links_id')->references('id')->on('store_links')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
