<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScrapperController;
use App\Http\Controllers\BarcodeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', [ScrapperController::class, 'index']);
Route::get('/search', [ScrapperController::class, 'search']);
Route::post('/', [ScrapperController::class, 'store']);

Route::get('/links/{id}', [ScrapperController::class, 'links']);
Route::get('/links/{id}/search', [ScrapperController::class, 'linksSearch']);
Route::post('/save-link', [ScrapperController::class, 'saveLink']);
Route::get('delete-link/{id}', [ScrapperController::class, 'deleteLink']);

Route::get('products/{id}', [ScrapperController::class, 'products']);
Route::get('products/{id}/search', [ScrapperController::class, 'productSearch']);

Route::post('getBarcode', [ScrapperController::class, 'getBarcode']);

Route::get('barcodes/{id}', [BarcodeController::class, '']);