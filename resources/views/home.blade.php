
@extends('layout.main')

@section('content')

    @php
        $searchTerm = isset($searchTerm)?$searchTerm:'';

    @endphp


    <div class="container">

        <div class="row">
            <div class="col-lg-10 mx-auto my-4">

                <form class="form-inline my-2 my-lg-0" action="/search" method="GET" role="search">
                    <input value="{{$searchTerm}}" class="form-control mr-sm-2" type="search" placeholder="Search by name" aria-label="Search" name="title" autocomplete="off">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    <br>
                    @if ($searchTerm != '')
                        <a href="/" class="p-3">Clear Search</a>
                    @endif
                </form>


                <hr>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#storeModal" onclick="addStore()">Add Store</button>
                <br>
                <br>
                <div class="filter-result">

                    @if(count($stores) == 0)
                        <div class="job-box d-md-flex align-items-center justify-content-between mb-30">
                            <div class="job-left my-4 align-items-center">
                                <div class="job-content d-flex">
                                    <label class="text-center text-md-left mx-2">No Store found</label>
                                </div>
                            </div>
                        </div>
                    @endif

                    @foreach($stores as $store)
                        <div class="job-box d-md-flex align-items-center justify-content-between mb-30">
                            <div class="job-left my-4 d-md-flex align-items-center flex-wrap">
                                <div class="job-content">
                                    <h5 class="text-center text-md-left">{{ $store['title'] }}</h5>
                                </div>
                            </div>
                            <div class="d-flex">
                                <a href="links/{{$store['id']}}" class="btn btn-light mx-2">Links {{$store['total']}}</a>
                                <button class="btn btn-secondary" data-toggle="modal" data-target="#storeModal" onclick="editStore({{$store}})">Edit</button>
                            </div>
                        </div>
                    @endforeach

                        <div class="float-right">
                            <p> Showing Records {{$stores->count()}} of {{$stores->total()}}</p>
                        </div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                @if ($stores->previousPageUrl())
                                    <li class="page-item"><a class="page-link" href="{{$stores->previousPageUrl().'&title='.$searchTerm}}">Previous</a></li>
                                @endif
                                @for ($i = 1; $i <= $stores->lastPage(); $i++)
                                    <li class="page-item  {{$stores->currentPage() === $i ? 'active':''}}"><a class="page-link" href="{{$stores->url($i).'&title='.$searchTerm}}">{{$i}}</a></li>
                                @endfor
                                    @if ($stores->nextPageUrl())
                                        <li class="page-item"><a class="page-link" href="{{$stores->nextPageUrl().'&title='.$searchTerm}}">Next</a></li>
                                    @endif
                            </ul>
                        </nav>




                    <!-- The Modal -->
                    <div class="modal fade" id="storeModal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Add Store</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <form id="store-form" action="/" method="post">
                                        @csrf
                                        <input type="text" id="title" name="title" class="border rounded w-100 p-2" placeholder="Enter store name...">
                                        <input type="hidden" id="id" name="id">
                                    </form>

                                </div>

                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" onclick="submitForm()">Save</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>

    </div>
@endsection

@section('footer')
    <script type="application/javascript">

        function submitForm() {
            let form = document.getElementById("store-form");
            form.submit()
        }
        function searchStore() {
            let form = document.getElementById("store-form");
            form.submit()
        }

        function editStore(store) {
            document.getElementById("id").value = store.id;
            document.getElementById("title").value = store.title;
        }

        function addStore() {
            document.getElementById("id").value = '';
            document.getElementById("title").value = '';
        }

    </script>
@endsection