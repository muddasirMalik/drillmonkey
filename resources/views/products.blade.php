@extends('layout.main')
@section('head-style')
    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }

        .custom-image {
            width: 15vw;
        }

        .custom-card {
            width: 20vw;
            max-height: 30vw;
            min-height: 25vw;
            background: #f1f1f1;
        }
    </style>
@endsection

@section('store')
    {{ $storeLink->store->title }}
@endsection
@section('content')

    @php
        $searchTerm = isset($searchTerm)?$searchTerm:'';
        $id = isset($id)?$id:'';
    @endphp

    <div class="container min-h-screen bg-gray-100 dark:bg-gray-900">

        <div class="row">
            <div class="col-lg-10 mx-auto my-4 ">

                <form class="form-inline my-2 my-lg-0" action="/products/{{$id}}/search" method="GET" role="search">
                    @csrf
                    <input value="{{$searchTerm}}" class="form-control mr-sm-2" type="search"
                           placeholder="Search by name" aria-label="Search" name="name" autocomplete="off">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    <br>
                    @if ($searchTerm != '')
                        <a href="/products/{{$id}}" class="p-3">Clear Search</a>
                    @endif
                    <a href="/links/{{ $storeLink->store_id }}" class="btn btn-secondary ml-auto">Back</a>
                </form>

                <hr>


{{--                <h6>Scrapped Pages: {{ $storeLink->scrapped_pages }} of {{ $storeLink->total_pages }}</h6>--}}
{{--                <div class="d-flex">--}}
{{--                    @if($storeLink->scrapped_pages == $storeLink->total_pages || $storeLink->scrapped_pages > $storeLink->total_pages)--}}
{{--                        <a href="#" class="btn btn-info" style="pointer-events: none">Scrape Next Page</a>--}}
{{--                    @else--}}
{{--                        <a href="/products/{{ $storeLink->id }}?page_no={{ $storeLink->scrapped_pages + 1 }}"--}}
{{--                           class="btn btn-primary">Scrape Next Page</a>--}}
{{--                    @endif--}}
{{--                </div>--}}

                <hr>

                <div>
                    <p> Showing Records {{$items->count()}} of {{$items->total()}}</p>
                </div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        @if ($items->previousPageUrl())
                            <li class="page-item"><a class="page-link" href="{{$items->previousPageUrl().'&title='.$searchTerm}}">Previous</a>
                            </li>
                        @endif
                        @for ($i = 1; $i <= $items->lastPage(); $i++)
                                    <li class="page-item  {{$items->currentPage() === $i ? 'active':''}}"><a class="page-link" href="{{$items->url($i).'&title='.$searchTerm}}">{{$i}}</a></li>
                        @endfor
                        @if ($items->nextPageUrl())
                            <li class="page-item"><a class="page-link" href="{{$items->nextPageUrl().'&title='.$searchTerm}}">Next</a>
                            </li>
                        @endif
                    </ul>

                </nav>


                <br>
                <br>

                <div class="filter-result">

                    @if(count($items) == 0)
                        <div class="job-box d-md-flex align-items-center justify-content-between mb-30">
                            <div class="job-left my-4 align-items-center w-100">
                                <div class="job-content d-flex">
                                    <label class="text-center text-md-left mx-2">No Product found</label>
                                </div>
                            </div>
                        </div>
                    @endif



                    @foreach($items as $item)
                        <div class="job-box d-md-flex align-items-center justify-content-between mb-30">

                            <div class="job-left my-4 align-items-center w-100">

                                <div class="card text-center">

                                    <a href="{{ $item['link'] }}"  target="_blank" class="text-dark text-decoration-none">
                                        <div class="card-body overflow-auto">

                                            <img class="custom-image float-left mx-auto" style="width: 16%" src="{{ $item['image'] }}"/>

                                            <label class="font-weight-bold">{{ $item['name'] }}</label>
                                            <label class="d-block text-center"><b>{{ $item['price'] }}</b></label>
                                            <label class="d-block text-center" id="barcode-{{ $item['id'] }}"><b>{{ $item['barcode'] }}</b></label>
                                        </div>
                                    </a>

                                    <div class="m-3 text-right">
{{--                                        <button class="btn btn-primary" onclick="getBarcode({{ $item['id'] }} )">Barcode List</button>--}}
                                        <a href="/barcodes/{{ $item['id'] }}" class="btn btn-primary" onclick="getBarcode({{ $item['id'] }} )">Barcode List</a>
                                    </div>
                                    <div>
                                        <button class="show-description btn btn-light w-100" title="Click Me!">Show Description</button>
                                        <div class="description text-center d-none">{!! $item['description'] !!}</div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    @endforeach

                </div>


            </div>
        </div>

    </div>



@endsection

@section('footer')
    <script type="application/javascript">

        function getBarcode(product){

            let barcodeLbl = $('#barcode-'+product);

            $.ajax({
                url: "{{ url('getBarcode') }}",
                data: {"_token" : "{{ csrf_token() }}",
                "id": product
                },
                type: 'post',
                success: function (response) {
                    let products = response.products;
                    if (products.length == 1)
                        barcodeLbl.text(products[0].barcode_number)
                    console.log(products);
                }
            })
        }


        Array.prototype.forEach.call(document.querySelectorAll('.show-description'), function addClickListener(btn) {
            btn.addEventListener('click', function (event) {
                var panel = this.nextElementSibling;
                if (panel.classList.contains("d-none")) {
                    panel.classList.remove("d-none");
                    this.textContent = "Hide Description";
                } else {
                    panel.classList.add("d-none");
                    this.textContent = "Show Description";
                }

            });
        });

    </script>
@endsection
