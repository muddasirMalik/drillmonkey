@extends('layout.main')

@section('store')
    {{ $store->title }}
@endsection
@section('content')

    @php
        $searchTerm = isset($searchTerm) ? $searchTerm:'';
        $id = isset($store->id) ? $store->id : '';
    @endphp

    <div class="container min-h-screen bg-gray-100 dark:bg-gray-900">

        <div class="row">
            <div class="col-lg-10 mx-auto my-4 ">

                <form class="form-inline my-2 my-lg-0" action="/links/{{$store->id}}/search" method="GET" role="search">
                    @csrf
                    <input value="{{$searchTerm}}" class="form-control mr-sm-2" type="search" placeholder="Search by category" aria-label="Search" name="name" autocomplete="off">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    <br>
                    @if ($searchTerm != '')
                        <a href="/links/{{$store->id}}" class="p-3">Clear Search</a>
                    @endif
                    <a href="/" class="btn btn-secondary ml-auto">Back</a>
                </form>

                <hr>
                <button class="btn btn-primary" data-toggle="modal" data-target="#linkModal" onclick="addLink()">Add Link</button>


                <div class="filter-result my-4">

                    @if(count($links) == 0)
                        <div class="job-box d-md-flex align-items-center justify-content-between mb-30">
                            <div class="job-left my-4 align-items-center">
                                <div class="job-content d-flex">
                                    <label class="text-center text-md-left mx-2">No link found</label>
                                </div>
                            </div>
                        </div>
                    @endif



                    @foreach($links as $link)
                        <div class="job-box d-md-flex align-items-center justify-content-between mb-30">

                            <div class="job-left my-4 align-items-center">

                                <div class="job-content d-flex">
                                    <h6>Name </h6>
                                    <label class="text-center text-md-left mx-2">{{ $link['name'] }}</label>
                                </div>

                                <div class="job-content d-flex">
                                    <h6>Link </h6>
                                    <a href="{{ $link['link'] }}" target="_blank" class="text-center text-md-left mx-2">{{ $link['link'] }}</a>
                                </div>


                            </div>
                            <div class="d-flex">
                                <button class="btn d-block w-100 d-sm-inline-block btn-light mx-2" data-toggle="modal" data-target="#linkModal" onclick="editLink({{ $link }})">Edit</button>
                                <a href="/products/{{ $link['id'] }}" class="btn btn-warning mx-2">Products</a>
                                <a href="/delete-link/{{ $link['id'] }}?redirect_id={{$store->id}}" class="btn d-block w-100 d-sm-inline-block btn-danger">Delete</a>
                            </div>
                        </div>
                    @endforeach

                        <div class="float-right">
                            <p> Showing Records {{$links->count()}} of {{$links->total()}}</p>
                        </div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                @if ($links->previousPageUrl())
                                    <li class="page-item"><a class="page-link" href="{{$links->previousPageUrl().'&title='.$searchTerm}}">Previous</a></li>
                                @endif
                                @for ($i = 1; $i <= $links->lastPage(); $i++)
                                    <li class="page-item  {{$links->currentPage() === $i ? 'active':''}}"><a class="page-link" href="{{$links->url($i).'&title='.$searchTerm}}">{{$i}}</a></li>
                                @endfor
                                    @if ($links->nextPageUrl())
                                        <li class="page-item"><a class="page-link" href="{{$links->nextPageUrl().'&title='.$searchTerm}}">Next</a></li>
                                    @endif
                            </ul>
                        </nav>



                    <!-- The Modal -->
                    <div class="modal fade" id="linkModal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Add Link</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <form id="store-link-form" action="/save-link" method="post">
                                        @csrf
                                        <input type="text" id="link" name="link" class="border rounded w-100 p-2 my-2" placeholder="https://www.example.com/category/">
{{--                                        <input type="text" id="type" name="type" class="border rounded w-100 p-2 " placeholder="Type" required>--}}
                                        <input type="text" id="name" name="name" class="border rounded w-100 p-2 my-2" placeholder="Name">
                                        <input type="hidden" id="store_id" name="store_id" value="{{ request()->route('id') }}">
                                        <input type="hidden" id="id" name="id">
                                    </form>

                                </div>

                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" onclick="submitForm()">Save</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>

    </div>
@endsection
@section('footer')
    <script type="application/javascript">

        function submitForm() {
            let form = document.getElementById("store-link-form");
            form.submit()
        }

        function editLink(obj) {
            document.getElementById('id').value = obj.id
            document.getElementById('link').value = obj.link
            document.getElementById('name').value = obj.name
            // document.getElementById('type').value = obj.type;
            // document.getElementById('type').style.display = 'none';
        }

        function addLink() {
            document.getElementById('id').value = ''
            document.getElementById('link').value = ''
            document.getElementById('name').value = ''
            // document.getElementById('type').value = ''
            // document.getElementById('type').style.display = 'block';
        }

        document.addEventListener("DOMContentLoaded", function(event) {
            document.getElementById("link").addEventListener('change', function(){

                var str = document.getElementById('link').value
                var arr = str.split('/')
                if(arr.includes('www.thetoyshop.com')){
                    document.getElementById('name').value = arr[arr.length-1]
                }
                if(arr.includes('www.dunelm.com')){
                    document.getElementById('name').value = arr[arr.length-1]
                }
                if(arr.includes('www.wayfair.co.uk')){
                    document.getElementById('name').value = arr[arr.length-1].replace('.html','')
                }
            });
        });

    </script>
@endsection

