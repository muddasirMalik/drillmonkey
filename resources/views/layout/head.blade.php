<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Match Monkey</title>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

<!-- Styles -->
<style>
    body {
        background: #f5f5f5;
        margin-top: 20px;
    }

    .filter-result .job-box {
        -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
        box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
        border-radius: 10px;
        padding: 10px 35px;
    }

    ul {
        list-style: none;
        padding: 0 !important;
    }

    .list-disk li {
        list-style: none;
        margin-bottom: 12px;
    }

    .list-disk li:last-child {
        margin-bottom: 0;
    }

    .job-content ul li {
        font-weight: 600;
        opacity: 0.75;
        border-bottom: 1px solid #ccc;
        padding: 10px 5px;
    }

    @media (min-width: 768px) {
        .job-content ul li {
            border-bottom: 0;
            padding: 0;
        }
    }

    .job-content ul li i {
        font-size: 20px;
        position: relative;
        top: 1px;
    }

    .mb-30 {
        margin-bottom: 30px;
    }

</style>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<link href="{{ asset('css/app.css') }}" rel="stylesheet">