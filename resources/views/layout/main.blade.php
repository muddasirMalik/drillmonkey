<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.head')
    @yield('head-style')
</head>
<body class="antialiased">
@include('layout.nav')
@yield('content')
</body>
@yield('footer')
</html>
