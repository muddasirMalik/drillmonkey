<?php

namespace App\Jobs;

use App\Models\Products;
use Goutte\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WayFairStoreJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $product = null;
//    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $page = $client->request('GET', env('PROXY_CRAWL_JS_TOKEN') . "&page_wait=2000&url=" . urlencode($this->product->link));
        $imageSection = $page->filter('.ProductDetailSingleMediaItem')->first();
        if ($imageSection->count() > 0 && $imageSection->filter('img')->count() > 0) {
            $this->product->image = $imageSection->filter('img')->attr('src');
        }

        if ($this->product->image == '')
            $this->product->image = "no image";
//            $this->release();

//        $description = $page->filter('.ProductOverviewInformation')->first();
//        $this->product->description = $description->html();
        $description = $page->filter('#js-pdp-section-product-overview');
        if ($description->count() > 0)
            $this->product->description = $description->html();


        $this->product = json_decode(json_encode($this->product), true); // Object to Array Conversion
        Products::updateOrCreate(['id' => $this->product['id']], $this->product);
    }

    /**
     * Calculate the number of seconds to wait before retrying the job.
     *
     * @return array
     */
//    public function backoff()
//    {
//        return [20, 60, 120];
//    }
}
