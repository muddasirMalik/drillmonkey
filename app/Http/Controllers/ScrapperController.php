<?php

namespace App\Http\Controllers;

use App\Jobs\DunelmStoreJob;
use App\Jobs\ToyStoreJob;
use App\Jobs\WayFairStoreJob;
use App\Models\Products;
use App\Models\Store;
use App\Models\StoreLinks;
use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;
use Illuminate\Database\QueryException;

//use Symfony\Component\CssSelector\CssSelectorConverter;

class ScrapperController extends Controller
{
    public function index()
    {
        $stores = Store::latest()->paginate(5);
        return view('home', ['stores' => $stores]);
    }

    public function search()
    {
        $searchTerm = request('title');
        $stores = Store::query()
            ->where('title', 'LIKE', "%{$searchTerm}%")
            ->paginate(5);
        return view('home', ['stores' => $stores, 'searchTerm' => $searchTerm]);

    }

    public function store()
    {

        $store = Store::updateOrCreate(['id' => request('id')], request()->post());

        return redirect('/');
    }


    public function links(Request $request)
    {
        $store = Store::with('links')->findOrFail($request->id);
        $storeLinks = $store->links()->paginate(10);
        return view('store_links', ['links' => $storeLinks, 'id' => $request->id, 'store' => $store]);
    }

    public function linksSearch(Request $request)
    {
        $searchTerm = request('name');

        $store = Store::find($request->id);

        $storeLinks = StoreLinks::where([
            ['store_id', $request->id],
            ['name', 'like', '%'."$searchTerm".'%']
        ])->latest()->paginate(10);


        return view('store_links', ['links' => $storeLinks, 'searchTerm' => $searchTerm, 'store' => $store]);

    }

    public function saveLink()
    {
        $data = request()->post();

        if (strpos(request('link'),'toyshop') > -1)
            $data['type'] = "toyshop";
        if (strpos(request('link'),'dunelm') > -1)
            $data['type'] = "dunelm";
        if (strpos(request('link'),'wayfair') > -1)
            $data['type'] = "wayfair";
        
        $store = Store::findOrFail(request('store_id'));
        $link = StoreLinks::updateOrCreate(['id' => request('id')], $data);
        $store->links()->save($link);

        return redirect('/links/' . request('store_id'));
    }

    public function deleteLink()
    {
        $store = StoreLinks::find(request()->route('id'));
        $store->delete();
        return redirect('/links/'.request('redirect_id'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function products(Request $request)
    {

        $storeLink = StoreLinks::with('products')->find($request->id);

        if ($storeLink->products->count() > 0 && !$request->page_no) {
            $products = $storeLink->products()->paginate(10);
        } else {
            if ($storeLink->type == 'ToyShop' || $storeLink->type == 'toyshop')
                $this->toyProducts($storeLink, $request->page_no);
            elseif ($storeLink->type == 'Dunelm' || $storeLink->type == 'dunelm')
                $this->dunelmProducts($storeLink, $request->page_no);
            elseif ($storeLink->type == 'Wayfair' || $storeLink->type == 'wayfair')
                $this->wayfairProducts($storeLink, $request->page_no);

            $storeLink = $storeLink->fresh();
            $products = $storeLink->products()->paginate(10);
        }

        return view('products', ['items' => $products, 'id' => $request->id, 'storeLink' => $storeLink]);
    }

    public function productSearch(Request $request)
    {
        $searchTerm = request('name');

        $storeLink = StoreLinks::find($request->id);

        $products = Products::where([['store_links_id', $request->id], ['name', 'like', "%".$searchTerm."%"]])->paginate(10);

        return view('products', ['items' => $products, 'searchTerm' => $searchTerm, 'id' => $request->id, 'storeLink' => $storeLink]);
    }

    /**
     * @param $store
     * @return array
     */
    public function toyProducts($store, $page_no = 1)
    {
        try {
            // URL
            $prefix = "https://www.thetoyshop.com";
            $url = $store->link;

//            if ($page_no > 1) {
//                $page_no = (int)$page_no - 1;
//                $url .= "?page=" . $page_no; // Store pagination starts from 0 (e.g Page 3 link would be www.abc.com?page=2)
//            }

            $client = new Client();
            $page = $client->request('GET', $url);


//            // Pagination
//            if ($store->total_pages == 0) {
//                $pagination = $page->filter(".pagination-bar")->first();
//                $pagination = $pagination->filter(".pagination > li");
//                if ($pagination->count()) {
////                    $pagination = $pagination->filter(".pagination > li");
//                    $pagination = $pagination->count() - 2; // -2 for next and prev buttons
//                    $store->total_pages = $pagination;
//                    $store->scrapped_pages = 1;
//                }
//                else{
//                    $store->total_pages = 1;
//                    $store->scrapped_pages = 1;
//                }
//                $store->save();
//            }
//
//            // Only increment in page when page no is greater than 1
//            if ($page_no > 1 && $store->scrapped_pages < $store->total_pages){
//                // Increase scrape page count
//                $store->scrapped_pages += 1;
//                $store->save();
//            }


            // loop through the data
            $items = $page->filter('.product-item')->each(function (Crawler $node, $i) use ($prefix) {

                // search for values
                $img = $node->filter('div > a > img')->attr('src');
                $name = $node->filter('div > .details > a')->text();
                $price = $node->filter('div > .details > .grid-pricing > .price')->text();
                $link = $node->filter('div > .details > a')->attr('href');

                $img = $prefix . $img;
                $link = $prefix . $link;

                $item = ['image' => $img,
                    'name' => $name,
                    'price' => $price,
                    'link' => $link,
                ];

                return $item;

            });

            // store values in db
            foreach ($items as $item) {
                $product = Products::updateOrCreate(['name' => $item['name'], 'link' => $item['link'], 'image' => $item['image']], $item);
                $product->storeLink()->associate($store);
                $product->save();
//                ToyStoreJob::dispatch($product)->delay(now()->addMinutes(1));
            }

            return $items;
        } catch (QueryException $e) {
            return dd($e);
        }

    }

    /**
     * @param $store
     * @return array
     */
    public function dunelmProducts($store, $page_no = 1)
    {
        try{
            // url
            $prefix = "https://www.dunelm.com";
            $url = $store->link;
            $proxyUrl = env('PROXY_CRAWL_JS_TOKEN') . "&url=" . $url;

//            if ($page_no > 1)
//                $proxyUrl .= "&page=" . $page_no;

            $client = new Client();
            $page = $client->request('GET', $proxyUrl);

            // Pagination
//            if ($store->total_pages == 0) {
//                $pagination = $page->filter('.dw-6d6ggt-SelectElement--Select')->last();
//                if($pagination->attr('aria-label') == "Pagination"){
//                    $pagination = $pagination->filter('option');
//                    $store->total_pages = $pagination->count();
//                    $store->scrapped_pages = 1;
//                }
//                else{
//                    $store->total_pages = 1;
//                    $store->scrapped_pages = 1;
//                }
//                $store->save();
//            }

            // Only increment in page when page no is greater than 1
//            if ($page_no > 1 && $store->scrapped_pages < $store->total_pages){
//                // Increase scrape page count
//                $store->scrapped_pages += 1;
//                $store->save();
//            }


            // loop through the data
            $items = $page->filter('.e1gxusoh1')->each(function (Crawler $node, $i) use ($prefix, $client) {

                // search for values
                $img = $node->filter('a > div > div > img')->attr('src');

                if($node->filter('a > .e18cnc5n0 > p')->count() > 1)
                    $name = $node->filter('a > .e18cnc5n0 > p')->last()->text("No Name");
                else
                    $name = $node->filter('a > .e18cnc5n0 > p')->text("No Name");

                $price = $node->filter('a > .e18cnc5n0 > .e18cnc5n4 > p')->text("No Price");
                $link = $node->filter('a')->attr('href');

                $link = $prefix . $link;

                // If image is base64 then get image from Product Page
                if (strpos($img, 'base64') !== false) {
                    $productPage = $client->request('GET', $link);
                    $productPage = $productPage->filter('.e1nvizg33')->first();

                    if ($productPage->count() > 0) {
                        $imageSrc = $productPage->filter('img')->attr('src');
                        $img = $imageSrc;
                    }
                }

                $item = ['image' => $img,
                    'name' => $name,
                    'price' => $price,
                    'link' => $link,
                ];

                return $item;
            });
            
            // store values in db
            foreach ($items as $key => $item) {

//                $product = Products::updateOrCreate(['name' => $item['name'], 'image' => $item['image'], 'link' => $item['link'], 'price' => $item['price']], $item);
                $product = Products::create($item);
                $product->storeLink()->associate($store);
                $product->save();
//                DunelmStoreJob::dispatch($product)->delay(now()->addMinutes(1));
            }

            return $items;
        }
        catch (QueryException $e) {
            return dd($e);
        }

    }

    /**
     * @param $store
     * @return array
     */
    public function wayfairProducts($store, $page_no = 1)
    {
        try{
            ini_set("max_execution_time", -1);

            // url
            $url = $store->link;
            $proxyUrl = env('PROXY_CRAWL_JS_TOKEN') . "&page_wait=3000&url=" . $url;

//            if ($page_no > 1)
//                $proxyUrl .= "&page=" . $page_no;

            $client = new Client();
            $page = $client->request('GET', $proxyUrl);

//            // Pagination
//            if ($store->total_pages == 0) {
//                $pagination = $page->filter(".pl-Pagination");
//                $pagination = $pagination->filter("span")->last();
//                if ($pagination->count()) {
//                    $pagination = $pagination->text();
//                    $store->total_pages = (int)$pagination;
//                    $store->scrapped_pages = 1;
//                }
//                else{
//                    $store->total_pages = 1;
//                    $store->scrapped_pages = 1;
//                }
//                $store->save();
//            }
//
//            // Only increment in page when page no is greater than 1
//            if ($page_no > 1 && $store->scrapped_pages < $store->total_pages){
//                // Increase scrape page count
//                $store->scrapped_pages += 1;
//                $store->save();
//            }


            // loop through the data
            $items = $page->filter('.ProductCard-container')->each(function (Crawler $node, $i) use ($client) {

                // search for values
                $link = $node->filter('a')->attr('href');
                $img = $node->filter('a > .ProductCard-imageWrap > div > div > img');

                // Get Image from product page
                if ($img->count() == 0) {
                    $img = "";
                } else {
                    $img = $img->attr('src');
                }

                $name = $node->filter('a > .ProductCard-details > .ProductCard-header > h2')->text();
                $price = $node->filter('a > .ProductCard-details > .ProductCard-pricing > .ProductCard-price')->text();
                preg_match_all('!\d+!', $price, $matches);

                if (strpos($price, '$') > -1)
                    $price = '$ ' . $matches[0][0];
                elseif (strpos($price, '£') > -1)
                    $price = '£ ' . $matches[0][0];

                $item = ['image' => $img,
                    'name' => $name,
                    'price' => $price,
                    'link' => $link,
                ];

                return $item;
            });

            // store values in db
            foreach ($items as $key => $item) {

                $product = Products::updateOrCreate(['name' => $item['name'], 'image' => $item['image'], 'link' => $item['link']], $item);
                $product->storeLink()->associate($store);
                $product->save();
                // If image is empty get image through a job
//                if ($product->image == '')
//                WayFairStoreJob::dispatch($product)->delay(now()->addMinutes(1));
            }

            return $items;
        }
        catch (QueryException $e) {
            return dd($e);
        }

    }


    public function getBarcode(Request $request){

        try{

            $product = Products::find($request->id);
            $title = urlencode($product->name);
            $title = urlencode("Memphis Day Bed Only Ivory");

            $api_key = '0rf5fmj49b5xlth4bwl0daychih1cd';
            $url = 'https://api.barcodelookup.com/v3/products?title='.$title.'&formatted=y&key=' . $api_key;


            $ch = curl_init(); // Use only one cURL connection for multiple queries

            $data = $this->get_data($url, $ch);

            $response = json_decode($data);

            return response()->json($response);
        }
        catch (\Exception $exception){
            return response()->json($exception);
        }

    }

    function get_data($url, $ch) {

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    function productDataApi(){
        $request = new Http_Re('https://api.apigenius.io/products/search');
    }

}
