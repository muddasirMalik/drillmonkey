<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $guarded = ['id'];

    public function storeLink(){
        return $this->belongsTo(StoreLinks::class, 'store_links_id');
    }

}
