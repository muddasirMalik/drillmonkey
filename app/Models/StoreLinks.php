<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreLinks extends Model
{
    protected $guarded = ['id'];

    public function store(){
        return $this->belongsTo(Store::class);
    }

    public function Products(){
        return $this->hasMany(Products::class);
    }

}
